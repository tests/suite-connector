#!/bin/bash

set -ex

SC_PACKAGE=golang-github-eclipse-kanto-suite-connector
ARCHITECTURES=(amd64 armhf arm64)

OUT="$PWD"

goarch_for_arch() {
  local arch="$1"
  [[ "$arch" == "armhf" ]] && arch=arm
  echo "$arch"
}

sudo apt-get build-dep -y "$SC_PACKAGE"

export PATH=/usr/lib/go-1.18/bin:$PATH

git clone --depth=1 -b c2esetup https://gitlab.apertis.org/tests/kanto-c2esetup.git
pushd kanto-c2esetup/integration/c2esetup
for arch in "${ARCHITECTURES[@]}"; do
  GOARCH=$(goarch_for_arch $arch) go build -v -o "$OUT/$arch/bin/c2esetup"
done
popd

git clone --depth=1 -b $osname/$release https://gitlab.apertis.org/pkg/"$SC_PACKAGE".git
git clone -b int_test https://gitlab.apertis.org/tests/suite-connector-mirror.git tests
cp -r tests/integration "$SC_PACKAGE"
pushd "$SC_PACKAGE"
dh_auto_configure --builddirectory=_build --buildsystem=golang
export GO111MODULE=off GOPATH=$PWD/_build
for arch in "${ARCHITECTURES[@]}"; do
  GOARCH=$(goarch_for_arch $arch) \
    go test -tags integration -c -o $OUT/$arch/bin/sc-test ./integration
done
popd

git add {amd64,armhf,arm64}
git config --local user.email testbot@apertis.org
git config --local user.name 'Apertis Test Bot'

if git diff --quiet --staged; then
  echo 'No changes to commit.'
  exit
fi

git commit -sm "Binary update suite-connector $(date +%Y%m%d)"
git show

if [[ "$DO_PUSH" == "1" ]]; then
  git push \
    https://${GITLAB_CI_USER}:${GITLAB_CI_PASSWORD}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git/ \
    HEAD:$BRANCH
fi
