# suite-connector

This contains the tests scripts, binaries, and build scripts for the Eclipse
Kanto suite-connector integration tests.

## Binaries

Two binaries are included in the repository:

- `c2esetup`: Runs on GitLab CI before the tests are submitted in order to set
  up the testing environment on the Apertis cloud2edge instance.
- `sc-test`: Contains the actual integration tests for Eclipse Kanto
  [suite-connector](https://github.com/eclipse-kanto/suite-connector/).

  At the moment, this uses a
  [mirror](https://gitlab.apertis.org/tests/suite-connector-mirror) of a *fork*
  of suite-connector that contains [yet-to-be-merged
  tests](https://github.com/eclipse-kanto/suite-connector/pull/45). Once they are
  merged in, the mirror will be removed.

The build process follows the pattern set by the
[helper-tools](https://gitlab.apertis.org/tests/helper-tools) repository, which
is used by other tests to pull testing binaries out of Apertis packages. In
this case, however, the build process is custom and does not actually use
helper-tools itself, because:

- We have no package for `c2esetup`, as that is not intended to be used from
  within Apertis itself.
- `sc-test` must be built in a special way due to intricacies with compiling
  standalone binaries from Go test suites. These build steps do not fit in very
  well with the standard Debian Go packaging flow and would require a moderate
  amount of effort to fix.

In order to rebuild the binaries, simply re-run the CI pipeline again.

## Testing Flow

The high-level flow for how the Kanto tests run is:

- `c2esetup` runs as part of the apertis-image-recipes GitLab pipeline, in order
  to set up cloud2edge.
- The pipeline submits the tests, which use `sc-test`, to LAVA.
- `sc-test` runs within LAVA to execute the actual tests.
- `c2esetup` runs again in the pipeline in order to clean up any created
  resources that the tests used.

