#!/bin/sh

set -e

CERT=/etc/suite-connector/apertis.crt
LOG=/var/log/suite-connector/suite-connector.log

error_occured() {
  echo "FAILED: $0"
  exit 1
}

trap error_occured EXIT

. common/update-test-path

openssl s_client -connect hono.apertis.dev:443 -showcerts </dev/null \
  | openssl x509 -outform PEM \
  | sudo tee $CERT >/dev/null

sudo tee /etc/suite-connector/config.json >/dev/null <<EOF
{
    "cacert": "$CERT",
    "logFile": "$LOG",
    "address": "mqtts://hono.apertis.dev:8883",
    "tenantId": "$TENANT",
    "deviceId": "$C2E_DEVICE_ID",
    "authId": "$(echo "$C2E_DEVICE_ID" | sed 's/:/_/g')",
    "password": "$C2E_DEVICE_PASSWORD"
}
EOF

sudo systemctl restart suite-connector.service

if ! sc-test -test.v; then
  rc=$?
  sudo cat $LOG || :
  exit $?
fi

trap '' EXIT
echo "PASSED: $0"
